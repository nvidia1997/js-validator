# @nvidia1997/js-validator

This is simple validation library written in typescript and compiled down to javascript.

#### If you need this for react app, please use this wrapper [react-js-validator](https://www.npmjs.com/package/@nvidia1997/react-js-validator).

## How to use:

### load the library:

##### For Node js

```javascript
const { Validator } = require("@nvidia1997/js-validator");
```

##### For environments that support "import" keyword

```javascript
import { Validator } from "@nvidia1997/js-validator";
```

##### Create new validator instance

```javascript
const mainValidator = new Validator();

// or you can pass a config
const mainValidatorExtended = new Validator({
  onStateChanged: (validatorId) => console.log("this validator was updated"),
  overrideOnIdConflict: true, // whether to override if there"s sub validator defined with this name.It throws if false
  throwIfNotValidated: false// whether to  throw exception if you try to access hasErrors or some other post validation related prop
});
```

##### Define some sub-validators:

### Important note !

> Using allowNull will skip execution of other sub-validations if passed passedValue === null
>
> Using allowUndefined will skip execution of other sub-validations if passed passedValue === undefined
>
> Using notRequired will skip execution of other sub-validations if passedValue === null || passedValue === undefined
>
> Most of the validation methods have extra param "flipLogic" (NOT required) which reverses the validation logic.
> Example: if we want to look for emails we use "email("Error message")", if we want to look for everything but emails, we use "email("Error message", true)"

```javascript
const flipLogic = false;
```

#### number

```javascript
 simpleValidator
  .number(12, "numberValidator", "Error: value is not a number")
  .allowNull() // null values won't trigger errors
  .allowUndefined() // undefined values won't trigger errors
  .notRequired() // allowNull and allowUndefined combined
  .greaterThan(3, "Error: value is not >3", flipLogic)
  .greaterThanOrEqual(5, "Error: value is not >=5", flipLogic)
  .lessThan(2, "Error: value is not <2", flipLogic)
  .lessThanOrEqual(2, "Error: value is not <=2", flipLogic)
  .exclusiveBetween(1, 4, "Error: value is not 1>value<4", flipLogic)
  .inclusiveBetween(1, 2, "Error: value is not 1>=value<=2", flipLogic)
  .notZero("Error: value must be !=0", flipLogic)
  .notInfinity("Error: value is not ", flipLogic)
  .odd("Error: value % 2 == 0", flipLogic)
  .even("Error: value % 2 != 0", flipLogic)
  .integer("Error: value is not integer", flipLogic)
  .canBeDividedBy(3, "Error: value % 3 != 0", flipLogic)
  .equalTo(6, "Error: value !== comparissonValue", flipLogic)
  .isUnique([3, 4], "Error: value already exists", flipLogic);
// .validate(); // you can call validate method directly after criteria definition or do it later as shown below
```

#### string

```javascript
 simpleValidator
  .string("George", "stringValidator", "Error: value is not a string")
  .allowNull() // null values won't trigger errors
  .allowUndefined() // undefined values won't trigger errors
  .notRequired() // allowNull and allowUndefined combined
  .maxLength(3, "Error: maxLength>3", flipLogic)
  .minLength(3, "Error: minLength<3", flipLogic)
  .matchesRegex(new RegExp("foo*"), "Error: regex not matching", flipLogic)
  .email("This email is not valid", flipLogic)
  .zipCode("GR", true, false,
    "Invalid ZIP code",
    flipLogic) // you can ignore whitespaces so "383 33" = "38333" and decide what to do when there are missing zip code rules
  .startsWith("mouse", "Error: text doesn't start with such prefix", flipLogic)
  .endsWith("mouse", "Error: text doesn't end with such suffix", flipLogic)
  .contains("or", "Error: text doesn't contain such part", flipLogic)
  .phoneNumber("BG", "Error: invalid phone number", flipLogic)
  .equalTo("George", "Error: value !== comparissonValue", flipLogic)
  .isUnique(["George", "Mary"], "Error: value already exists", flipLogic);
```

#### object

```javascript
  mainValidator
  .object({ users: [] }, "objectValidator", "Error: value is not an object") // defines new object sub validator
  .allowNull() // null values won't trigger errors
  .allowUndefined() // undefined values won't trigger errors
  .notRequired() // allowNull and allowUndefined combined
  .hasProp("users", "Error: object has no such prop", flipLogic)
  .isPropSet("car", "Error: value must be !== undefined && !== null", flipLogic)
  .notEmpty("Error: object has not props", flipLogic)
  .equalTo({}, "Error: value !== comparissonValue", flipLogic)
  .isUnique([obj1, obj2], "Error: value already exists", flipLogic);
```

#### boolean

```javascript
mainValidator
  .boolean(true, "booleanVlidator", "Error: value is not a boolean") // defines new boolean sub validator
  .allowNull() // null values won't trigger errors
  .allowUndefined() // undefined values won't trigger errors
  .notRequired() // allowNull and allowUndefined combined
  .isTrue("Error: value !=true", flipLogic)
  .isFalse("Error: value !=false", flipLogic)
  .equalTo(true, "Error: value !== comparissonValue", flipLogic)
  .isUnique([true, false], "Error: value already exists", flipLogic);
```

##### get access to specific sub validator

```javascript
const ageValidator = mainValidator.getValidator("ageValidator") // if the validator doesn"t exists it returns undefined
```

##### validate

```javascript
//you can validate all sub validators at once by
mainValidator.validate();

// or to validate only specific validator using
const specificValidator = mainValidator.getValidator("ageValidator") // if the validator doesn"t exists it returns undefined
specificValidator.validate(); //this will automatically invoke onState changed function if available
specificValidator.validate(false); //this will NOT invoke onState changed function
```

##### errors

```javascript
const someValidator = mainValidator.getValidator("someValidator");

someValidator.errors;// Returns errors array. This prop can be used on main validator or sub validators. Each error has name and message.
mainValidator.bulkErrors("someValidator1", "someValidator2", "someValidator3"); // combines the errors of multiple sub-validators

someValidator.hasErrors; // This prop can be used on main validator or sub validators
mainValidator.bulkHasErrors("someValidator1", "someValidator2"); // combines the result of multiple sub-validators

someValidator.firstErrorMessage; // retrieves the first error message
mainValidator.bulkFirstErrorMessage("someValidator1", "someValidator2"); // retrieves the first error message of the sub-validators group
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://gitlab.com/nvidia1997/js-validator/LICENSE)

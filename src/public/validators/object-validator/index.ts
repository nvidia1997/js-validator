import {SPECIAL_ERRORS, Validator} from "../internal";
import {ERRORS} from "./constants";

export class ObjectValidator extends Validator {
    constructor(value: object, validatorId: string, generalErrorMessage?: string) {
        super(value, validatorId, generalErrorMessage);
        this.isOfValidType = typeof this.value === "object" && this.value !== null; // null is object
        this.typeErrorName = SPECIAL_ERRORS.NOT_OBJECT;
        this.validateType();
    }

    public notEmpty(errorMessage?: string, flipLogic: boolean = false): ObjectValidator {
        return this._handleValidation({
            validationCallback: this.notEmpty,
            validationCallbackArgs: arguments,
            hasError: Object.keys(this.value).length === 0,
            errorName: ERRORS.NOT_EMPTY,
            errorMessage,
            flipLogic,
        });
    }

    public hasProp(propName: string, errorMessage?: string, flipLogic: boolean = false): ObjectValidator {
        return this._handleValidation({
            validationCallback: this.hasProp,
            validationCallbackArgs: arguments,
            hasError: this.value[propName] === undefined,
            errorName: ERRORS.HAS_PROP,
            errorMessage,
            flipLogic,
        });
    }

    public isPropSet(propName: string, errorMessage?: string, flipLogic: boolean = false): ObjectValidator {
        return this._handleValidation({
            validationCallback: this.isPropSet,
            validationCallbackArgs: arguments,
            hasError: this.value[propName] === undefined || this.value[propName] === null,
            errorName: ERRORS.IS_PROP_SET,
            errorMessage,
            flipLogic,
        });
    }
}

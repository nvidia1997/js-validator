export const ERRORS = {
    NOT_EMPTY: "notEmpty",
    HAS_PROP: "hasProp",
    IS_PROP_SET: "isPropSet",
}
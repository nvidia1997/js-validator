import {SPECIAL_ERRORS, Validator} from "../internal";
import {ERRORS} from "./constants";

export class BooleanValidator extends Validator {
    constructor(value: boolean, validatorId: string, generalErrorMessage?: string) {
        super(value, validatorId, generalErrorMessage);
        this.isOfValidType = typeof this.value === "boolean";
        this.typeErrorName = SPECIAL_ERRORS.NOT_BOOLEAN;
        this.validateType();
    }

    public isTrue(errorMessage?: string, flipLogic: boolean = false): BooleanValidator {
        return this._handleValidation({
            validationCallback: this.isTrue,
            validationCallbackArgs: arguments,
            hasError: this.value !== true,
            errorName: ERRORS.IS_TRUE,
            errorMessage,
            flipLogic,
        });
    }

    public isFalse(errorMessage?: string, flipLogic: boolean = false): BooleanValidator {
        return this._handleValidation({
            validationCallback: this.isFalse,
            validationCallbackArgs: arguments,
            hasError: this.value !== false,
            errorName: ERRORS.IS_FALSE,
            errorMessage,
            flipLogic
        });
    }
}

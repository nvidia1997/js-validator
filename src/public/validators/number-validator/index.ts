import {Validator, SPECIAL_ERRORS} from "../internal";
import {ERRORS} from "./constants";

export class NumberValidator extends Validator {
    constructor(value: number, validatorId: string, generalErrorMessage?: string) {
        super(value, validatorId, generalErrorMessage);
        this.isOfValidType = typeof this.value === "number";
        this.typeErrorName = SPECIAL_ERRORS.NOT_NUMBER;
        this.validateType();
    }

    public greaterThan(val: number, errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.greaterThan,
            validationCallbackArgs: arguments,
            hasError: this.value <= val,
            errorName: ERRORS.GREATER_THAN,
            errorMessage,
            flipLogic
        });
    }

    public greaterThanOrEqual(val: number, errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.greaterThanOrEqual,
            validationCallbackArgs: arguments,
            hasError: this.value < val,
            errorName: ERRORS.GREATER_THAN_OR_EQUAL,
            errorMessage,
            flipLogic
        });
    }

    public lessThan(val: number, errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.lessThan,
            validationCallbackArgs: arguments,
            hasError: this.value >= val,
            errorName: ERRORS.LESS_THAN,
            errorMessage,
            flipLogic
        });
    }

    public lessThanOrEqual(val: number, errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.lessThanOrEqual,
            validationCallbackArgs: arguments,
            hasError: this.value > val,
            errorName: ERRORS.LESS_THAN_OR_EQUAL,
            errorMessage,
            flipLogic
        });
    }

    /**
     * @description value CAN'T be equal to min and max
     */
    public exclusiveBetween(minValue: number, maxValue: number, errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.exclusiveBetween,
            validationCallbackArgs: arguments,
            hasError: this.value <= minValue || this.value >= maxValue,
            errorName: ERRORS.EXCLUSIVE_BETWEEN,
            errorMessage,
            flipLogic
        });
    }

    /**
     * @description value CAN be equal to min and max
     */
    public inclusiveBetween(minValue: number, maxValue: number, errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.inclusiveBetween,
            validationCallbackArgs: arguments,
            hasError: this.value < minValue || this.value > maxValue,
            errorName: ERRORS.INCLUSIVE_BETWEEN,
            errorMessage,
            flipLogic
        });
    }

    public notZero(errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.notZero,
            validationCallbackArgs: arguments,
            hasError: this.value === 0,
            errorName: ERRORS.NOT_ZERO,
            errorMessage, flipLogic
        });
    }

    public notInfinity(errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.notInfinity,
            validationCallbackArgs: arguments,
            hasError: this.value === Number.POSITIVE_INFINITY || this.value === Number.NEGATIVE_INFINITY,
            errorName: ERRORS.NOT_INFINITY,
            errorMessage,
            flipLogic
        });
    }

    public odd(errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.odd,
            validationCallbackArgs: arguments,
            hasError: this.value % 2 === 0,
            errorName: ERRORS.ODD,
            errorMessage,
            flipLogic
        });
    }

    public even(errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.even,
            validationCallbackArgs: arguments,
            hasError: this.value % 2 !== 0,
            errorName: ERRORS.EVEN,
            errorMessage,
            flipLogic
        });
    }

    public integer(errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.integer,
            validationCallbackArgs: arguments,
            hasError: !Number.isInteger(this.value),
            errorName: ERRORS.INTEGER,
            errorMessage,
            flipLogic
        });
    }

    public canBeDividedBy(val: number, errorMessage?: string, flipLogic: boolean = false): NumberValidator {
        return this._handleValidation({
            validationCallback: this.canBeDividedBy,
            validationCallbackArgs: arguments,
            hasError: this.value % val !== 0,
            errorName: ERRORS.CAN_BE_DIVIDED_BY,
            errorMessage,
            flipLogic
        });
    }
}

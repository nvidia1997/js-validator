export const ERRORS = {
    GREATER_THAN: "greaterThan",
    GREATER_THAN_OR_EQUAL: "greaterThanOrEqual",
    LESS_THAN: "lessThan",
    LESS_THAN_OR_EQUAL: "lessThanOrEqual",
    EXCLUSIVE_BETWEEN: "exclusiveBetween",
    INCLUSIVE_BETWEEN: "inclusiveBetween",
    NOT_ZERO: "notZero",
    NOT_INFINITY: "notInfinity",
    ODD: "odd",
    EVEN: "even",
    INTEGER: "integer",
    CAN_BE_DIVIDED_BY: "canBeDividedBy",
}

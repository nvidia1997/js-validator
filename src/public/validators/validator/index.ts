import {
    BaseValidator,
    ValidationError,
} from "../internal";
import {ERRORS} from "./constants";
import {specialErrorsNames} from "./utils";


export class Validator extends BaseValidator {
    private readonly _id: string;
    private readonly _generalErrorMessage?: string;
    private readonly _value: any;
    private _pendingValidations: ((...args: any[]) => Validator)[] = [];
    private _isValidating: boolean = false;
    private _isValidated: boolean = false;
    private _allowNull: boolean = false;
    private _allowUndefined: boolean = false;
    private _typeErrorName?: string;
    private _isValidType?: boolean;

    constructor(value: any, validatorId: string, generalErrorMessage?: string) {
        super();

        this.validateId(validatorId);
        this._id = validatorId;
        this._generalErrorMessage = generalErrorMessage;
        this._value = value;
    }

    public get id() {
        return this._id;
    }

    protected get typeErrorName(): string {
        if (typeof this._typeErrorName !== "string") {
            throw  new Error("Property not initialized");
        }

        return this._typeErrorName;
    }

    protected set typeErrorName(value: string) {
        this._typeErrorName = value;
    }

    protected validateType() {
        return this._handleValidation({
            validationCallback: this.validateType,
            validationCallbackArgs: arguments,
            hasError: !this.isOfValidType,
            errorName: this.typeErrorName,
            errorMessage: this.generalErrorMessage,
            flipLogic: false
        });
    }

    protected get isOfValidType(): boolean {
        if (typeof this._isValidType !== "boolean") {
            throw  new Error("Property not initialized");
        }

        return this._isValidType;
    }

    protected set isOfValidType(value: boolean) {
        if (typeof value !== "boolean") {
            throw  new TypeError("Invalid parameter type");
        }

        this._isValidType = value;
    }

    public get isValidated(): boolean {
        return this._isValidated;
    }

    private _filterErrors(_errors: ValidationError[]) {
        if (_errors.length === 0) {
            return _errors;
        }

        if (this._allowNull && this.value === null || this._allowUndefined && this.value === undefined) {
            return [];
        }

        const _specialErrors = _errors.filter(
            ({name}: ValidationError) => specialErrorsNames.includes(name));

        return _specialErrors.length > 0 ? _specialErrors : _errors;
    }

    private _resetPendingValidations() {
        this._pendingValidations = [];
    }

    public get errors(): ValidationError[] {
        if (this._options.throwIfNotValidated && !this._isValidated) {
            throw new Error(`You haven't executed validation method on: ${this.id}`);
        }

        return this._filterErrors(this._errors);
    }

    protected get generalErrorMessage() {
        return this._generalErrorMessage;
    }

    protected get pendingValidations() {
        return this._pendingValidations;
    }

    protected get isValidating(): boolean {
        return this._isValidating;
    }

    public validate(fireOnStateChanged: boolean = true): void {
        this._isValidating = true;
        for (let i = 0; i < this.pendingValidations.length; i++) {
            this.pendingValidations[i]();
        }
        this._isValidating = false;
        this._isValidated = true;
        this._resetPendingValidations();

        if (fireOnStateChanged) {
            this.onStateChanged(this.id);
        }
    }

    /** @description removes all validation errors from current validator*/
    public reset(fireOnStateChanged: boolean = true): void {
        this._errors = [];
        this._isValidated = false;
        this._allowNull = false;
        this._allowUndefined = false;
        this._resetPendingValidations();

        if (fireOnStateChanged) {
            this.onStateChanged(this.id);
        }
    }

    protected get value() {
        return this._value;
    }

    protected _addError(name: string, message = this._generalErrorMessage) {
        if (this._errors.some(e => e.name === name)) {
            this._errors = this._errors.filter(e => e.name !== name);
        }
        this._errors.push({name, message, validatorId: this.id});
    }

    private _addValidation(callback: (...params: any[]) => Validator, callbackArgs: IArguments) {
        const _callback = callback.bind(this, ...Array.from(callbackArgs));
        this.pendingValidations.push(_callback);
    }

    protected _handleValidation(args: {
        validationCallback: (...params: any[]) => Validator,
        validationCallbackArgs: IArguments,
        hasError: boolean,
        flipLogic: boolean,
        errorName: string,
        errorMessage: string | undefined
    }) {
        const _hasError = args.flipLogic
            ? !args.hasError
            : args.hasError;

        if (this.isValidating) {
            if (_hasError) {
                this._addError(args.errorName, args.errorMessage);
            }
        }
        else {
            this._addValidation(args.validationCallback, args.validationCallbackArgs);
        }

        return this;
    }

    public allowNull() {
        this._allowNull = true;

        return this;
    }

    public allowUndefined() {
        this._allowUndefined = true;

        return this;
    }

    public notRequired() {
        this.allowNull();
        this.allowUndefined();

        return this;
    }

    public equalTo(comparisonValue: any, errorMessage?: string, flipLogic: boolean = false) {
        return this._handleValidation({
            validationCallback: this.equalTo,
            validationCallbackArgs: arguments,
            hasError: this.value !== comparisonValue,
            errorName: ERRORS.EQUAL_TO,
            errorMessage,
            flipLogic
        });
    }

    public isUnique(values: any[], errorMessage?: string, flipLogic: boolean = false) {
        const _isUnique: boolean = values
            .filter(_v => _v === this.value)
            .length <= 1;

        return this._handleValidation({
            validationCallback: this.isUnique,
            validationCallbackArgs: arguments,
            hasError: !_isUnique,
            errorName: ERRORS.IS_UNIQUE,
            errorMessage,
            flipLogic
        });
    }
}

import {SPECIAL_ERRORS} from "../constants";

export const specialErrorsNames: string[] = Object.values(SPECIAL_ERRORS);
export const SPECIAL_ERRORS = {
    NOT_STRING: "notString",
    NOT_NUMBER: "notNumber",
    NOT_BOOLEAN: "notBoolean",
    NOT_OBJECT: "notObject",
};

export const ERRORS = {
    EQUAL_TO: "equalTo",
    IS_UNIQUE: "isUnique",
};

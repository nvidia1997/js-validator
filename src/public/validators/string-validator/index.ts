import {CountryCode} from "libphonenumber-js";
import {SPECIAL_ERRORS, Validator} from "../internal";
import {emailRegex, ERRORS} from "./constants";
import {isValidPhoneNumber} from "./utils/phone.utils";
import {isValidZipCode} from "./utils/zip-code.utils";

export class StringValidator extends Validator {
    constructor(value: string, validatorId: string, generalErrorMessage?: string) {
        super(value, validatorId, generalErrorMessage);
        this.isOfValidType = typeof this.value === "string";
        this.typeErrorName = SPECIAL_ERRORS.NOT_STRING;
        this.validateType();
    }

    public notEmpty(errorMessage?: string, flipLogic: boolean = false): StringValidator {
        return this._handleValidation({
            validationCallback: this.notEmpty,
            validationCallbackArgs: arguments,
            hasError: this.value.trim() === "",
            errorName: ERRORS.NOT_EMPTY,
            errorMessage,
            flipLogic
        });
    }

    public minLength(length: number, errorMessage?: string, flipLogic: boolean = false): StringValidator {
        return this._handleValidation({
            validationCallback: this.minLength,
            validationCallbackArgs: arguments,
            hasError: this.value.length < length,
            errorName: ERRORS.MIN_LENGTH,
            errorMessage,
            flipLogic
        });
    }

    public maxLength(length: number, errorMessage?: string, flipLogic: boolean = false): StringValidator {
        return this._handleValidation({
            validationCallback: this.maxLength,
            validationCallbackArgs: arguments,
            hasError: this.value.length > length,
            errorName: ERRORS.MAX_LENGTH,
            errorMessage,
            flipLogic
        });
    }

    public matchesRegex(regExp: RegExp, errorMessage?: string, flipLogic: boolean = false): StringValidator {
        return this._handleValidation({
            validationCallback: this.matchesRegex,
            validationCallbackArgs: arguments,
            hasError: !regExp.test(this.value),
            errorName: ERRORS.MATCHES_REGEX,
            errorMessage,
            flipLogic
        });
    }

    public email(errorMessage?: string, flipLogic: boolean = false): StringValidator {
        const regExp = new RegExp(emailRegex);

        return this._handleValidation({
            validationCallback: this.email,
            validationCallbackArgs: arguments,
            hasError: !regExp.test(this.value),
            errorName: ERRORS.EMAIL,
            errorMessage,
            flipLogic
        });
    }

    /**
     * @param {string} countryCode - case insensitive Alpha-2 ISO code - example: "US"
     * @param ignoreWhitespace
     * @param throwOnMissingZipCodeDefinitions if there are no zipcode rules for selected country it throws error, otherwise zipcode is counted as valid
     * @param errorMessage
     * @param flipLogic
     */
    public zipCode(countryCode: string, ignoreWhitespace: boolean = true, throwOnMissingZipCodeDefinitions: boolean = false, errorMessage?: string, flipLogic: boolean = false): StringValidator {
        return this._handleValidation({
            validationCallback: this.zipCode,
            validationCallbackArgs: arguments,
            hasError: !isValidZipCode(this.value, countryCode, ignoreWhitespace, throwOnMissingZipCodeDefinitions),
            errorName: ERRORS.ZIP_CODE,
            errorMessage,
            flipLogic
        });
    }

    public startsWith(val: string, errorMessage?: string, flipLogic: boolean = false): StringValidator {
        return this._handleValidation({
            validationCallback: this.startsWith,
            validationCallbackArgs: arguments,
            hasError: !this.value.startsWith(val),
            errorName: ERRORS.STARTS_WITH,
            errorMessage,
            flipLogic
        });
    }

    public endsWith(val: string, errorMessage?: string, flipLogic: boolean = false): StringValidator {
        return this._handleValidation({
            validationCallback: this.endsWith,
            validationCallbackArgs: arguments,
            hasError: !this.value.endsWith(val),
            errorName: ERRORS.ENDS_WITH,
            errorMessage,
            flipLogic
        });
    }

    public contains(val: string, errorMessage?: string, flipLogic: boolean = false): StringValidator {
        return this._handleValidation({
            validationCallback: this.contains,
            validationCallbackArgs: arguments,
            hasError: !this.value.includes(val),
            errorName: ERRORS.CONTAINS,
            errorMessage,
            flipLogic
        });
    }

    public phoneNumber(countryCode?: CountryCode, errorMessage?: string, flipLogic: boolean = false): StringValidator {
        return this._handleValidation({
            validationCallback: this.phoneNumber,
            validationCallbackArgs: arguments,
            hasError: !isValidPhoneNumber(this.value, countryCode),
            errorName: ERRORS.PHONE_NUMBER,
            errorMessage,
            flipLogic
        });
    }
}

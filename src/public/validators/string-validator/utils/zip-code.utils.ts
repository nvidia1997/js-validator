import {StringUtils} from "../../../utils/string-utils";

/**
 * @param zipcode
 * @param {string} countryCode - case insensitive Alpha-2 ISO code - example: "US"
 * @param ignoreWhitespace
 * @param throwOnMissingZipCodeDefinitions
 */
export function isValidZipCode(zipcode: string, countryCode: string, ignoreWhitespace: boolean, throwOnMissingZipCodeDefinitions: boolean) {
    if (typeof zipcode !== "string") {
        throw new TypeError("Expected a string for zipcode");
    }

    const _countryCode = typeof countryCode === "string"
        ? StringUtils.replaceAllWhitespaces(countryCode, "").toUpperCase()
        : undefined;

    if (!_countryCode || countryCode.length !== 2) {
        throw new TypeError("Invalid country code");
    }

    // @ts-ignore
    const _zipCodesRegex: { [key: string]: string | number | undefined } = require("zipcodes-regex");
    let _zipCodeRegex = _zipCodesRegex[_countryCode];

    if (typeof _zipCodeRegex !== "string") {
        if (!throwOnMissingZipCodeDefinitions) {
            return true;
        }

        throw new TypeError("There is no regular expression for the country you selected");
    }

    let _zipCode: string = zipcode;

    if (ignoreWhitespace) {
        _zipCodeRegex = StringUtils.replaceAllWhitespaces(_zipCodeRegex.toString(), "");
        _zipCode = StringUtils.replaceAllWhitespaces(_zipCode, "");
    }

    return new RegExp(_zipCodeRegex).test(_zipCode);
}

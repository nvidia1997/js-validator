import {CountryCode} from 'libphonenumber-js';

export function isValidPhoneNumber(phoneNumber: string, countryCode?: CountryCode): boolean {
    if (typeof countryCode === "string" && countryCode.trim().length !== 2) {
        throw  new Error("Invalid countryCode");
    }

    if (
        typeof phoneNumber !== "string"
        || !phoneNumber.startsWith("+")
        || phoneNumber.length < 3
    ) {
        return false;
    }

    const _parsePhoneNumber = require('libphonenumber-js').default;
    const _result = _parsePhoneNumber(
        phoneNumber,
        {
            defaultCountry: countryCode,
            extract: false
        }
    );

    if (countryCode && countryCode !== _result?.country) {
        return false;
    }


    return _result?.isValid() || false;
}

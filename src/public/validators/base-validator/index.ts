import {
    BooleanValidator,
    NumberValidator,
    ObjectValidator,
    StringValidator,
    Validator,
} from "../internal";


export interface ValidationError {
    name: string,
    message?: string,
    validatorId: string,
}

interface ValidatorOptions {
    throwIfNotValidated?: boolean,
    overrideOnIdConflict?: boolean,
    onStateChanged?: (validatorId?: string) => any,
}


export class BaseValidator {
    protected _options: ValidatorOptions = {
        throwIfNotValidated: false,
        overrideOnIdConflict: false,
        onStateChanged: undefined,
    };
    private _validators: Validator[] = [];
    protected _errors: ValidationError[] = [];


    constructor(options: ValidatorOptions = {}) {
        if (typeof options !== "object") {
            return;
        }

        if (options.throwIfNotValidated) {
            this._options.throwIfNotValidated = options.throwIfNotValidated;
        }

        if (options.overrideOnIdConflict) {
            this._options.overrideOnIdConflict = options.overrideOnIdConflict;
        }

        if (options.onStateChanged && typeof options.onStateChanged !== "function") {
            throw new Error("onStateChanged param has to be a function");
        }
        else if (options.onStateChanged) {
            this._options.onStateChanged = options.onStateChanged;
        }
    }

    public onStateChanged(validatorId?: string) {
        if (this._options.onStateChanged) {
            this._options.onStateChanged(validatorId);
        }
    }

    public get validators() {
        return this._validators;
    }

    public get errors(): ValidationError[] {
        return this.validators.flatMap(
            (v: Validator) => v.errors);
    }

    public get hasErrors(): boolean {
        return this.errors.length > 0;
    }

    /**
     * @description combines the errors of specified validators into array
     */
    public bulkErrors(...validatorIds: string[]): ValidationError[] {
        this._bulkValidateIds(validatorIds);
        return this.errors.filter(e => validatorIds.includes(e.validatorId));
    }

    /**@description checks if some of the selected validators have errors*/
    public bulkHasErrors(...validatorIds: string[]): boolean {
        this._bulkValidateIds(validatorIds);
        return this.bulkErrors(...validatorIds).length > 0;
    }

    private _getFirstErrorMessage(selectedErrors: ValidationError[]): string | undefined {
        return selectedErrors[0] && selectedErrors[0].message;
    }

    /**@description combines error messages of selected validators and returns the first one*/
    public bulkFirstErrorMessage(...validatorIds: string[]) {
        return this._getFirstErrorMessage(this.bulkErrors(...validatorIds));
    }

    public get firstErrorMessage() {
        return this._getFirstErrorMessage(this.errors);
    }

    public validate(fireOnStateChanged: boolean = true): void {
        for (let i = 0; i < this.validators.length; i++) {
            this.validators[i].validate(false);
        }

        if (fireOnStateChanged) {
            this.onStateChanged(undefined);
        }
    }

    /** @description removes all validation errors from all child validators*/
    public reset(fireOnStateChanged: boolean = true): void {
        for (let i = 0; i < this.validators.length; i++) {
            this.validators[i].reset(false);
        }

        if (fireOnStateChanged) {
            this.onStateChanged(undefined);
        }
    }

    private _bulkValidateIds(validatorIds: string[]) {
        if (!validatorIds.length) {
            this.validateId(undefined);
        }

        for (const vId of validatorIds) {
            this.validateId(vId);
        }
    }

    protected validateId(validatorId: any) {
        if (typeof validatorId !== "string" || validatorId.trim().length === 0) {
            throw new Error("validator id must be non empty string");
        }
    }

    public getValidator(validatorId: string) {
        this.validateId(validatorId);
        return this.validators.find(v => v.id === validatorId);
    }

    private _addValidator(_validator: Validator) {
        const isBusy = this.validators.map(v => v.id).some(id => id === _validator.id);
        if (isBusy) {
            if (!this._options.overrideOnIdConflict) {
                throw new Error(`There's other validator with id: ${_validator.id}`);
            }

            this.removeValidator(_validator.id);
        }

        _validator._options = this._options;
        this.validators.push(_validator);
    }

    public removeValidator(validatorId: string): BaseValidator {
        this.validateId(validatorId);
        this._validators = this.validators.filter(v => v.id !== validatorId);

        return this;
    }

    public string(value: string, validatorId: string, generalErrorMessage?: string): StringValidator {
        const _validator = new StringValidator(value, validatorId, generalErrorMessage);
        this._addValidator(_validator);

        return _validator;
    }

    public number(value: number, validatorId: string, generalErrorMessage?: string): NumberValidator {
        const _validator = new NumberValidator(value, validatorId, generalErrorMessage);
        this._addValidator(_validator);

        return _validator;
    }

    public object(value: object, validatorId: string, generalErrorMessage?: string): ObjectValidator {
        const _validator = new ObjectValidator(value, validatorId, generalErrorMessage);
        this._addValidator(_validator);

        return _validator;
    }

    public boolean(value: boolean, validatorId: string, generalErrorMessage?: string): BooleanValidator {
        const _validator = new BooleanValidator(value, validatorId, generalErrorMessage);
        this._addValidator(_validator);

        return _validator;
    }
}


export class StringUtils {
    static replaceAllWhitespaces(text: string, replaceValue: string): string {
        return text.replace(/\s/gi, replaceValue);
    }
}
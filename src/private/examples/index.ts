import {Validator} from "../../public/index";

function test() {
    const simpleValidator = new Validator();

    simpleValidator
        .number(12, "age")
        .greaterThan(13);
    // .validate();

    simpleValidator
        .string("wazaa", "messageInput")
        .maxLength(3);

    simpleValidator
        .boolean(true, "booleanValidator")
        .isTrue();

    simpleValidator
        .string("table football", "regexValidator1")
        .matchesRegex(new RegExp("foo*"));

    simpleValidator
        .string("someEmail@gmail.com", "emailValidator")
        .email('This email is not valid');

    simpleValidator
        .string("38333", "zipCodeValidator")
        .zipCode("GR", false, false, "Invalid ZIP code");

    simpleValidator
        .string("383 3 3", "zipCodeValidator2")
        .zipCode("GR", true, false, "Invalid ZIP code");

    simpleValidator
        .string("383 3 3", "zipCodeValidator3")
        .zipCode("TT", true, false, "Invalid ZIP code");

    simpleValidator
        .number(3, "validator.isUnique.1")
        .isUnique([1, 2, 3, 3, 4, 5, 6]);

    simpleValidator.validate();

    return;
}

test();

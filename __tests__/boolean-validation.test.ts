import {Validator} from "../src/public";

test("boolean.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        // @ts-ignore
        .boolean("aaa", "booleanValidator")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("boolean.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .boolean(true, "booleanValidator")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("boolean.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .boolean(false, "booleanValidator")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("boolean.isTrue.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .boolean(true, "boolean.isTrueValidator")
        .isTrue()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("boolean.isTrue.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .boolean(false, "boolean.isTrueValidator")
        .isTrue()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("boolean.isFalse.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .boolean(false, "boolean.isFalseValidator")
        .isFalse()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("boolean.isFalse.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .boolean(true, "boolean.isFalseValidator")
        .isFalse()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});
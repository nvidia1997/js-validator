import {Validator} from "../src/public";

test("object.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .object({e: 3}, "objectValidator")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("object.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        // @ts-ignore
        .object("thx", "objectValidator")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("object.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        // @ts-ignore
        .object(null, "objectValidator")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("object.4", () => {
    const simpleValidator = new Validator();
    simpleValidator
        // @ts-ignore
        .object(null, "objectValidator")
        .allowNull()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("object.4", () => {
    const simpleValidator = new Validator();
    simpleValidator
        // @ts-ignore
        .object(undefined, "objectValidator")
        .allowUndefined()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("object.5", () => {
    const simpleValidator = new Validator();
    simpleValidator
        // @ts-ignore
        .object(undefined, "objectValidator")
        .notRequired()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("object.6", () => {
    const simpleValidator = new Validator();
    simpleValidator
        // @ts-ignore
        .object(undefined, "objectValidator")
        .allowNull()
        .allowUndefined()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("object.notEmpty.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .object({}, "object.notEmptyValidator")
        .notEmpty()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("object.notEmpty.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .object({}, "object.notEmptyValidator")
        .notEmpty()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("object.notEmpty.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .object({a: 3}, "object.notEmptyValidator")
        .notEmpty()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("object.hasProp.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .object({}, "object.hasPropValidator")
        .hasProp("age")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("object.hasProp.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .object({age: 90}, "object.hasPropValidator")
        .hasProp("age")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("object.isPropSet.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .object({}, "object.isPropSetValidator")
        .isPropSet("car")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("object.isPropSet.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .object({car: null}, "object.isPropSetValidator")
        .isPropSet("car")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("object.isPropSet.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .object({car: undefined}, "object.isPropSetValidator")
        .isPropSet("car")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("object.isPropSet.4", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .object({car: "something != undefined and != null"}, "object.isPropSetValidator")
        .isPropSet("car")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});
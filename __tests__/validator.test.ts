import {Validator} from "../src/public";

test("validator.bulkHasErrors.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .boolean(true, "v1")
        .isTrue();

    simpleValidator
        .number(12, "v2")
        .greaterThan(12);

    simpleValidator
        .string("unga bunga", "v3")
        .maxLength(3);

    simpleValidator.validate();
    const result = simpleValidator.bulkHasErrors("v1", "v2", "v3");

    expect(result).toBeTruthy();
});

test("validator.bulkHasErrors.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .boolean(true, "v1")
        .isTrue();

    simpleValidator
        .number(12, "v2")
        .greaterThan(6);

    simpleValidator
        .string("unga bunga", "v3")
        .maxLength(3);

    simpleValidator.validate();
    const result = simpleValidator.bulkHasErrors("v1", "v2");

    expect(result).toBeFalsy();
});


test("validator.bulkHasErrors.3", () => {
    const simpleValidator = new Validator();
    simpleValidator.validate();

    let hasThrown = true;
    try {
        simpleValidator.bulkHasErrors();
    }
    catch (error) {
        hasThrown = true;
    }

    expect(hasThrown).toBeTruthy();
});

test("validator.bulkFirstErrorMessage.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .boolean(true, "v1")
        .isTrue("here1");

    simpleValidator
        .string("bunga", "v2")
        .maxLength(3, "here2");

    simpleValidator.validate();
    const result = simpleValidator.bulkFirstErrorMessage("v1", "v2");

    expect(result).toBe("here2");
});

test("validator.bulkFirstErrorMessage.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .boolean(false, "v1")
        .isTrue("here1");

    simpleValidator
        .string("bunga", "v2")
        .maxLength(3, "here2");

    simpleValidator.validate();
    const result = simpleValidator.bulkFirstErrorMessage("v1", "v2");

    expect(result).toBe("here1");
});

test("validator.equalTo.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("wazaa", "validator.equalToValidator")
        .equalTo("wazaa");

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeFalsy();
});

test("validator.equalTo.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("wazaa", "validator.equalToValidator")
        .equalTo("aa");

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeTruthy();
});

test("validator.equalTo.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(6, "validator.equalToValidator")
        .equalTo(6);

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeFalsy();
});

test("validator.equalTo.4", () => {
    const a = {};
    const simpleValidator = new Validator();
    simpleValidator
        .object(a, "validator.equalToValidator")
        .equalTo(a);

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeFalsy();
});

test("validator.equalTo.5", () => {
    const a = {};
    const simpleValidator = new Validator();
    simpleValidator
        .object(a, "validator.equalToValidator")
        .equalTo({});

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeTruthy();
});

test("validator.isUnique.0", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(3, "validator.isUnique.1")
        .isUnique([1, 2, 3]);

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeFalsy();
});

test("validator.isUnique.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(2, "validator.isUnique.2")
        .isUnique([1, 2, 3, 3]);

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeFalsy();
});

test("validator.isUnique.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(3, "validator.isUnique.2")
        .isUnique([1, 2, 3, 3]);

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeTruthy();
});

test("validator.isUnique.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("abba", "validator.isUnique.3")
        .isUnique(["Rick", "abba", "Mo"]);

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeFalsy();
});

test("validator.isUnique.4", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("abba", "validator.isUnique.4")
        .isUnique(["abba", "abba", "Mo"]);

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeTruthy();
});

test("validator.isUnique.5", () => {
    const a = {name: "val"};
    const b = {name: "re"};

    const simpleValidator = new Validator();
    simpleValidator
        .object(a, "validator.isUnique.5")
        .isUnique([a, b, a]);

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeTruthy();
});

test("validator.flipLogicTest.1", () => {
    const a = {name: "val"};
    const b = {name: "re"};

    const simpleValidator = new Validator();
    simpleValidator
        .object(a, "validator.flipLogicTest.1")
        .isUnique([a, b, a], undefined, true);

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeFalsy();
});

test("validator.flipLogicTest.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("long train", "validator.flipLogicTest.2")
        .startsWith("long", "Must NOT start with this", true);

    simpleValidator.validate();
    const result = simpleValidator.hasErrors;

    expect(result).toBeTruthy();
});

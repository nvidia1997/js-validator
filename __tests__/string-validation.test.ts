import {Validator} from "../src/public";

test("string.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("hello", "stringValidator")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        // @ts-ignore
        .string({e: 3}, "stringValidator")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.notEmpty.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("", "string.notEmptyValidator")
        .notEmpty()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.notEmpty.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("aa", "string.notEmptyValidator")
        .notEmpty()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.minLength.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("waz", "string.minLengthValidator")
        .minLength(3)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.minLength.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("wa", "string.minLengthValidator")
        .minLength(3)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.maxLength.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("waz", "string.maxLengthValidator")
        .maxLength(3)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.maxLength.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("wazaa", "string.maxLengthValidator")
        .maxLength(3)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.matchesRegex.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("table football", "string.matchesRegexValidator")
        .matchesRegex(new RegExp("fo*"))
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.matchesRegex.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("blah", "string.matchesRegexValidator")
        .matchesRegex(new RegExp("fo*"))
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.email.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("someEmail@gmail.com", "string.emailValidator")
        .email('This email is not valid')
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.email.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("randomtext", "string.emailValidator")
        .email('This email is not valid')
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.zipCode.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("383 33", "string.zipCodeValidator")
        .zipCode("GR", false, false, "Invalid ZIP code")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.zipCode.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("383 3 3", "string.zipCodeValidator")
        .zipCode("GR", false, false, "Invalid ZIP code")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.zipCode.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .string("383 3 3", "string.zipCodeValidator")
        .zipCode("GR", true, false, "Invalid ZIP code")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.zipCode.4", () => {
    const simpleValidator = new Validator();
    let _hasError;

    try {
        simpleValidator
            .string("383 3 3", "string.zipCodeValidator")
            .zipCode("TT", true, true, "Invalid ZIP code")
            .validate();
    }
    catch (error) {
        _hasError = true;
    }

    expect(_hasError).toBeTruthy();
});

test("string.zipCode.5", () => {
    const simpleValidator = new Validator();
    let _hasError;

    try {
        simpleValidator
            .string("383 3 3", "string.zipCodeValidator")
            .zipCode("TT", false, false, "Invalid ZIP code")
            .validate();
    }
    catch (error) {
        _hasError = true;
    }

    expect(_hasError).toBeFalsy();
});

test("string.startsWith.1", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("wazaaa", "string.startsWith.1")
        .startsWith("waz")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.startsWith.2", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("wazaaa", "string.startsWith.2")
        .startsWith("t")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.endsWith.1", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("wazaaa", "string.endsWith.1")
        .endsWith("aa")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.endsWith.2", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("wazaaa", "string.endsWith.2")
        .endsWith("t")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.contains.1", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("Jo Burziq", "string.contains.1")
        .contains("ur")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.contains.2", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("Jo Burziq", "string.contains.2")
        .contains("t")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.phoneNumber.1", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("invalid phone", "string.phoneNumber.1")
        .phoneNumber("BG")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.phoneNumber.2", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("35989545", "string.phoneNumber.2")
        .phoneNumber("BG")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.phoneNumber.3", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("089545", "string.phoneNumber.3")
        .phoneNumber("BG")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.phoneNumber.4", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("+35989545", "string.phoneNumber.4")
        .phoneNumber("BG")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.phoneNumber.5", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("+359895456487", "string.phoneNumber.5")
        .phoneNumber("BG")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("string.phoneNumber.6", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("359895456487", "string.phoneNumber.6")
        .phoneNumber("BG")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.phoneNumber.7", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("+359895456487", "string.phoneNumber.7")
        .phoneNumber("US")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("string.phoneNumber.8", () => {
    const simpleValidator = new Validator();

    simpleValidator
        .string("+359895456487", "string.phoneNumber.8")
        .phoneNumber()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

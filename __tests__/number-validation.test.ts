import {Validator} from "../src/public";

test("number.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(12, "numberValidator", "Not a number")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        // @ts-ignore
        .number("14", "numberValidator", "Not a number")
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.greaterThan.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(5, "number.greaterThanValidator")
        .greaterThan(3)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.greaterThan.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(3, "number.greaterThanValidator")
        .greaterThan(3)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.greaterThan.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(2, "number.greaterThanValidator")
        .greaterThan(3)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.greaterThanOrEqual.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(5, "number.greaterThanOrEqualValidator")
        .greaterThanOrEqual(5)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.greaterThanOrEqual.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(6, "number.greaterThanOrEqualValidator")
        .greaterThanOrEqual(5)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.greaterThanOrEqual.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(3, "number.greaterThanOrEqualValidator")
        .greaterThanOrEqual(5)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.lessThan.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(1, "number.lessThanValidator")
        .lessThan(2)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.lessThan.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(2, "number.lessThanValidator")
        .lessThan(2)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.lessThan.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(3, "number.lessThanValidator")
        .lessThan(2)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.lessThanOrEqual.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(2, "number.lessThanOrEqualValidator")
        .lessThanOrEqual(2)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.lessThanOrEqual.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(1, "number.lessThanOrEqualValidator")
        .lessThanOrEqual(2)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.lessThanOrEqual.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(3, "number.lessThanOrEqualValidator")
        .lessThanOrEqual(2)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.exclusiveBetween.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(2, "number.exclusiveBetweenValidator")
        .exclusiveBetween(1, 4)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.exclusiveBetween.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(1, "number.exclusiveBetweenValidator")
        .exclusiveBetween(1, 4)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.exclusiveBetween.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(0, "number.exclusiveBetweenValidator")
        .exclusiveBetween(1, 4)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.exclusiveBetween.4", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(4, "number.exclusiveBetweenValidator")
        .exclusiveBetween(1, 4)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.inclusiveBetween.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(2, "number.inclusiveBetweenValidator")
        .inclusiveBetween(1, 2)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.inclusiveBetween.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(0, "number.inclusiveBetweenValidator")
        .inclusiveBetween(1, 2)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.inclusiveBetween.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(3, "number.inclusiveBetweenValidator")
        .inclusiveBetween(1, 2)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.notZero.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(0, "number.notZeroValidator")
        .notZero()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.notZero.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(1, "number.notZeroValidator")
        .notZero()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.notInfinity.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(Number.POSITIVE_INFINITY, "number.notInfinityValidator")
        .notInfinity()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.notInfinity.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(Number.NEGATIVE_INFINITY, "number.notInfinityValidator")
        .notInfinity()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.notInfinity.3", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(4, "number.notInfinityValidator")
        .notInfinity()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.odd.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(4, "number.oddValidator")
        .odd()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.odd.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(3, "number.oddValidator")
        .odd()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.even.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(4, "number.evenValidator")
        .even()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.even.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(3, "number.evenValidator")
        .even()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.integer.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(1, "number.integerValidator")
        .integer()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.integer.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(1.99, "number.integerValidator")
        .integer()
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});

test("number.canBeDividedBy.1", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(3, "number.canBeDividedByValidator")
        .canBeDividedBy(3)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeFalsy();
});

test("number.canBeDividedBy.2", () => {
    const simpleValidator = new Validator();
    simpleValidator
        .number(3, "number.canBeDividedByValidator")
        .canBeDividedBy(2)
        .validate();

    const result = simpleValidator
        .hasErrors;

    expect(result).toBeTruthy();
});
